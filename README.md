# wix-ci

Proof of concept: build Windows MSI installer with GitLab-CI using WiX Toolset
and Wine inside a Docker container.

## How to use

1. [Fork](https://gitlab.com/harbottle/wix-ci/forks/new) this repo.
2. Replace `example.wxs` and `example2.wxs` with your own
[.wxs](http://wixtoolset.org/documentation/manual/v3/overview/files.html#additional-information)
file or files.
3. Commit and push

The resulting MSI package files will be available for download as artifacts of
the GitLab-CI pipeline triggered by your push.
